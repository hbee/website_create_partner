# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.
{
    'name': 'Odoo WTForms Examples',
    'version': '0.1',
    'author': 'HacBee UAB',
    'category': 'Custom',
    'website': 'http://www.hbee.eu',
    'description': """
""",
    'depends': [
        'website',
    ],
    'data': [
        'view/partners.xml',
    ],
    'installable': True,
    'application': False,
    'external_dependencies': {
        'python': ['speaklater', 'wtforms'],
    },

}
